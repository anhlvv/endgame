package com.example.graduateteam.endgame.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.graduateteam.endgame.R;
import com.example.graduateteam.endgame.ResponseAPI.Subjects;

import java.util.ArrayList;

public class SubjectAdapter extends ArrayAdapter<Subjects> {
    Context mContext;
    int mLayout;
    ArrayList<Subjects> mList;

    public SubjectAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<Subjects> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mLayout = resource;
        this.mList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View item = convertView;
        if(item == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            item = inflater.inflate(this.mLayout, null);
        }


        Subjects s = mList.get(position);

        TextView subject_name = (TextView) item.findViewById(R.id.subject_name);
        TextView subject_des = (TextView) item.findViewById(R.id.subject_des);
        TextView subject_createDate = (TextView) item.findViewById(R.id.subject_createDate);

        subject_name.setText(s.getName());
        subject_des.setText(s.getDescription());
        subject_createDate.setText(s.getCreateAt());

        return item;
    }
}
