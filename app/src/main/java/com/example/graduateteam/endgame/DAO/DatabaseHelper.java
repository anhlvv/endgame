package com.example.graduateteam.endgame.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "endgame.sqlite";
    private static final int DATABASE_VERSION = 1;

    private final String data_user = "CREATE TABLE data_user (\n" +
            "    id          INT, \n" +
            "    username        TEXT,\n" +
            "    password     TEXT,\n" +
            "    firstName TEXT,\n" +
            "    lastName TEXT,\n" +
            "    displayName TEXT,\n" +
            "    email TEXT,\n" +
            "    birthday TEXT,\n" +
            "    phone TEXT,\n" +
            "    sex INT,\n" +
            "    status BOOLEAN,\n" +
            "    createAt TEXT,\n" +
            "    updateAt TEXT,\n" +
            "    createBy INT,\n" +
            "    updateBy INT,\n" +
            "    roleId INT\n" +
            ");";

    private final String data_cart ="CREATE TABLE data_cart (\n" +
            "    id          INT, \n" +
            "    courseName        TEXT,\n" +
            "    courseId     INT,\n" +
            "    price INT \n" +
            ");";
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(data_user);
        db.execSQL(data_cart);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(data_user);
        db.execSQL(data_cart);
    }
}
