package com.example.graduateteam.endgame.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.graduateteam.endgame.R;
import com.example.graduateteam.endgame.ResponseAPI.CourseWares;
import com.example.graduateteam.endgame.ResponseAPI.Lessons;

import java.util.ArrayList;

public class CourseWareAdapter extends ArrayAdapter<CourseWares> {
    Context mContext;
    int mLayout;
    ArrayList<CourseWares> mList;

    public CourseWareAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<CourseWares> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mLayout = resource;
        this.mList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View item = convertView;
        if(item == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            item = inflater.inflate(this.mLayout, null);
        }


        CourseWares cw = mList.get(position);

        TextView cw_name = (TextView) item.findViewById(R.id.cw_name);
        TextView cw_des = (TextView) item.findViewById(R.id.cw_des);
        TextView cw_type = (TextView) item.findViewById(R.id.cw_type);
        TextView cw_createDate = (TextView) item.findViewById(R.id.cw_createDate);

        cw_name.setText(cw.getName());
        cw_des.setText(cw.getDescription());
        cw_type.setText(cw.getTypeName() + ": ");
        cw_createDate.setText(cw.getCreateAt());

        return item;
    }
}