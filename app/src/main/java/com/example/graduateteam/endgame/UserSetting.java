package com.example.graduateteam.endgame;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.graduateteam.endgame.Client.APIUtils;
import com.example.graduateteam.endgame.Client.DataClient;
import com.example.graduateteam.endgame.DAO.EGDAO;
import com.example.graduateteam.endgame.ResponseAPI.User;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserSetting extends AppCompatActivity {
    private Button btn_back;
    Intent intent = new Intent();
    TextInputEditText firstname, lastname, email, phone, birth;
    Button btnUpdate;
    RadioGroup radioSex;
    RadioButton rdoBtnSex, rdoMale, rdoFemale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_setting);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mapping();

        DataClient dataClient = APIUtils.getData();
        EGDAO egDao = new EGDAO(UserSetting.this);
        ArrayList<User> lstUser = egDao.read();
        User user = lstUser.get(0);
        Toast.makeText(this, "" + user.getId(), Toast.LENGTH_SHORT).show();
        firstname.setText(user.getFirstName());
        lastname.setText(user.getLastName());
        email.setText(user.getEmail());
        phone.setText(user.getPhone());
        birth.setText(user.getBirthday());
        if (user.getSex() == 1) {
            rdoMale.setChecked(true);
        } else {
            rdoMale.setChecked(true);
        }

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser();
            }
        });
    }

    public void mapping() {
        firstname = (TextInputEditText) findViewById(R.id.u_firstname);
        lastname = (TextInputEditText) findViewById(R.id.u_lastname);
        email = (TextInputEditText) findViewById(R.id.u_email);
        phone = (TextInputEditText) findViewById(R.id.u_phone);
        birth = (TextInputEditText) findViewById(R.id.u_birth);
        radioSex = (RadioGroup) findViewById(R.id.u_sex);
        rdoMale = (RadioButton) findViewById(R.id.rdoNam);
        rdoFemale = (RadioButton) findViewById(R.id.rdoNu);

        btnUpdate = (Button) findViewById(R.id.u_update);
    }

    public void updateUser() {
        DataClient dataClient = APIUtils.getData();
        EGDAO egDao = new EGDAO(UserSetting.this);
        ArrayList<User> lstUser = egDao.read();
        User user = lstUser.get(0);
        user.setFirstName(String.valueOf(firstname.getText()));
        user.setLastName(String.valueOf(lastname.getText()));
        user.setDisplayName(firstname.getText() + " " + lastname.getText());
        user.setEmail(String.valueOf(email.getText()));
        user.setPhone(String.valueOf(phone.getText()));
        user.setBirthday(String.valueOf(birth.getText()));
        int selectedId = radioSex.getCheckedRadioButtonId();

        rdoBtnSex = (RadioButton) findViewById(selectedId);
        user.setSex(rdoBtnSex.getText() == "male" ? 1 : 0);
//        user.setSex();
        Call<String> callback = dataClient.UpdateUser(user);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String update = (String) response.body();
                Log.d("123: ", "onResponse: " + update);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            intent.setClass(UserSetting.this, Home.class);
            startActivity(intent);
            UserSetting.this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
