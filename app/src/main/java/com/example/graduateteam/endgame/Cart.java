package com.example.graduateteam.endgame;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.graduateteam.endgame.Adapter.CartAdapter;
import com.example.graduateteam.endgame.Adapter.CourseWareAdapter;
import com.example.graduateteam.endgame.DAO.EGDAO;
import com.example.graduateteam.endgame.ResponseAPI.Carts;
import com.example.graduateteam.endgame.ResponseAPI.CourseWares;

import java.util.ArrayList;

public class Cart extends AppCompatActivity {
    ListView listCart;
    Button btnBack,btnAddOrder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        listCart = (ListView) findViewById(R.id.list_cart);
        btnBack = (Button) findViewById(R.id.btn_back) ;
        btnAddOrder =(Button) findViewById(R.id.btn_addOrder);
        final EGDAO egDao = new EGDAO(Cart.this);
        ArrayList<Carts> listCar = egDao.getAllCart() ;
        final CartAdapter adapter = new CartAdapter(Cart.this, R.layout.item_cart, listCar);
        listCart.setAdapter(adapter);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        btnAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean del = egDao.deleteAllCart();
                if(del){
                    finish();
                }
            }
        });

        listCart.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final Carts item = adapter.getItem(position);
                AlertDialog.Builder builder = new AlertDialog.Builder(Cart.this);
                builder.setTitle("Xóa");
                builder.setMessage("Bạn có muốn Xóa không?");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        egDao.deleteCart(item.getId());
                        ArrayList<Carts> listCar = egDao.getAllCart() ;
                        final CartAdapter adapter = new CartAdapter(Cart.this, R.layout.item_cart, listCar);
                        listCart.setAdapter(adapter);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return false;
            }
        });
    }

}
