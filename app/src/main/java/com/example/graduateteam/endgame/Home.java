package com.example.graduateteam.endgame;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.graduateteam.endgame.Adapter.CourseAdapter;
import com.example.graduateteam.endgame.Client.APIUtils;
import com.example.graduateteam.endgame.Client.DataClient;
import com.example.graduateteam.endgame.DAO.EGDAO;
import com.example.graduateteam.endgame.ResponseAPI.Carts;
import com.example.graduateteam.endgame.ResponseAPI.Course;
import com.example.graduateteam.endgame.ResponseAPI.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    SwipeRefreshLayout swipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        setTitle("Trang chủ");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        loadData();
//        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.simpleSwipeRefreshLayout);
//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                loadData();
//            }
//        });
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Cart.class);
                startActivity(intent);
            }
        });
    }

    private void loadData(){

        DataClient dataClient = APIUtils.getData();
        Call<List<Course>> callback = dataClient.CourseData();
        callback.enqueue(new Callback<List<Course>>() {
            @Override
            public void onResponse(Call<List<Course>> call, Response<List<Course>> response) {
                ArrayList<Course> arrCourse = (ArrayList<Course>) response.body();
                if (arrCourse.size() > 0){

                    ListView listView = (ListView) findViewById(R.id.list_course);
                    final CourseAdapter adapter = new CourseAdapter(Home.this, R.layout.item_course, arrCourse);
                    listView.setAdapter(adapter);

                    registerForContextMenu(listView);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            EGDAO egDao = new EGDAO(Home.this);
                            final Course item = adapter.getItem(position);
                            final ArrayList<Carts> listCart = egDao.getAllCart();

                            Toast.makeText(Home.this, "" + item.getId(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Home.this, Subject.class);
                            intent.putExtra("course_id", item.getId());
                            intent.putExtra("course_name", item.getName());
//                            //based on item add info to intent
                            startActivity(intent);
                            TextView txtPrice = (TextView) findViewById(R.id.course_price);
                            txtPrice.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    EGDAO egDao = new EGDAO(Home.this);
                                    final ArrayList<User> lstUser = egDao.read();
                                    ArrayList<Carts> lstCart = egDao.getAllCart();
                                    Carts c = new Carts(item.getName(),lstUser.get(0).getId(),item.getId(),item.getCoursePrice());
                                    long i = egDao.insertCart(c);
                                    if(i>0){
                                        Intent intent = new Intent(Home.this, Cart.class);
                                        intent.putExtra("course_id", item.getId());
                                        intent.putExtra("course_name", item.getName());
                                        intent.putExtra("user_id", lstUser.get(0).getId());
                                        intent.putExtra("course_price", item.getCoursePrice());
                                        startActivity(intent);
                                    }else{
                                        Toast.makeText(Home.this, "Them khong thanh cong", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<Course>> call, Throwable t) {
                Toast.makeText(Home.this, "khong co du lieu nao", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_userSetting) {
            Intent intent = new Intent(Home.this, UserSetting.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.logout) {
            final EGDAO egDao = new EGDAO( Home.this);
            final ArrayList<User> lstUser = egDao.read();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Logout");
            builder.setMessage("Bạn có muốn đăng xuất không?");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    boolean del = egDao.delete(lstUser.get(0).getId());
                    if(del){
                        Intent intent = new Intent(Home.this, Login.class);
                        startActivity(intent);
                        finish();
                    }
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
