package com.example.graduateteam.endgame.Client;

import com.example.graduateteam.endgame.ResponseAPI.Course;
import com.example.graduateteam.endgame.ResponseAPI.CourseWares;
import com.example.graduateteam.endgame.ResponseAPI.Lessons;
import com.example.graduateteam.endgame.ResponseAPI.Subjects;
import com.example.graduateteam.endgame.ResponseAPI.User;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DataClient {
    @GET("user/getUser/{username}/{password}")
    Call<User> LoginData(@Path("username") String username, @Path("password") String password);

    @GET("course/getAllCourse")
    Call<List<Course>> CourseData();

    @GET("subject/getAllSubjectByCourseId/{course_id}")
    Call<List<Subjects>> SubjectData(@Path("course_id") int id);

    @GET("lesson/getAllLessonBySubjectId/{subject_id}")
    Call<List<Lessons>> LessonData(@Path("subject_id") int id);

    @GET("courseware/getCourseWare/{lesson_id}")
    Call<List<CourseWares>> CourseWareData(@Path("lesson_id") int id);

    @GET("courseware/getVideo/{cwName}")
    Call<String> GetVideo(@Path("cwName") String cwName);

    @POST("user/updateUser")
    Call<String> UpdateUser(@Body User user);

    @POST("user/insertUser")
    Call<String> InsertUser(@Body User user);


}
