package com.example.graduateteam.endgame;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.graduateteam.endgame.Client.APIUtils;
import com.example.graduateteam.endgame.Client.DataClient;
import com.example.graduateteam.endgame.ResponseAPI.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {
    TextInputEditText firstname, lastname, email, password, username;
    Button btnRegister;
    RadioGroup radioSex;
    RadioButton rdoBtnSex, rdoMale, rdoFemale;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setTitle("Register");
        mapping();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User();
                user.setId(0);
                user.setFirstName(String.valueOf(firstname.getText()));
                user.setLastName(String.valueOf(lastname.getText()));
                user.setDisplayName(firstname.getText() + " " + lastname.getText());
                user.setEmail(String.valueOf(email.getText()));
                user.setPassword(String.valueOf(password.getText()));
                user.setUsername(String.valueOf(username.getText()));
                int selectedId = radioSex.getCheckedRadioButtonId();
                user.setRoleId(3);
                rdoBtnSex = (RadioButton) findViewById(selectedId);
                user.setSex(rdoBtnSex.getText() == "male" ? 1 : 0);
                DataClient dataClient = APIUtils.getData();

                Call<String> callback = dataClient.InsertUser(user);
                callback.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        String update = (String) response.body();
                        Log.d("123: ", "onResponse: " + update);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                    }
                });
            }
        });
    }

    public void mapping(){
        firstname = (TextInputEditText) findViewById(R.id.rFirst);
        lastname = (TextInputEditText) findViewById(R.id.rLast);
        email = (TextInputEditText) findViewById(R.id.rEmail);
        password = (TextInputEditText) findViewById(R.id.rPass);
        username = (TextInputEditText) findViewById(R.id.rName);
        radioSex = (RadioGroup) findViewById(R.id.rSex);
        rdoMale = (RadioButton) findViewById(R.id.rNam);
        rdoFemale = (RadioButton) findViewById(R.id.rNu);
        btnRegister = (Button) findViewById(R.id.rRegister);
    }
}
