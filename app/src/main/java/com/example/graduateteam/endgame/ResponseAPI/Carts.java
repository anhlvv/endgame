package com.example.graduateteam.endgame.ResponseAPI;

public class Carts {
    private Integer id;

    private String courseName;

    private int courseId;

    private int price;

    public Carts(String courseName,Integer id, int courseId, int price) {
        this.id = id;
        this.courseName = courseName;
        this.courseId = courseId;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }


    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
