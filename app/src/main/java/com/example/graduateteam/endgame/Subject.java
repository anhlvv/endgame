package com.example.graduateteam.endgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.graduateteam.endgame.Adapter.SubjectAdapter;
import com.example.graduateteam.endgame.Client.APIUtils;
import com.example.graduateteam.endgame.Client.DataClient;
import com.example.graduateteam.endgame.ResponseAPI.Subjects;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Subject extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);

        Intent intent = getIntent();
        String course_name = intent.getStringExtra("course_name");
        setTitle("Khóa học: " + course_name);
        int course_id = intent.getIntExtra("course_id", 0);
//        Toast.makeText(Subject.this, "" + course_id, Toast.LENGTH_SHORT).show();
        loadData(course_id);
    }

    private void loadData(int course_id) {

        DataClient dataClient = APIUtils.getData();
        Call<List<Subjects>> callback = dataClient.SubjectData(course_id);

        callback.enqueue(new Callback<List<Subjects>>() {
            @Override
            public void onResponse(Call<List<Subjects>> call, Response<List<Subjects>> response) {
                ArrayList<Subjects> arrSubject = (ArrayList<Subjects>) response.body();
                if (arrSubject.size() > 0) {
                    ListView listView = (ListView) findViewById(R.id.list_subject);
                    final SubjectAdapter adapter = new SubjectAdapter(Subject.this, R.layout.item_subject, arrSubject);
                    listView.setAdapter(adapter);

                    registerForContextMenu(listView);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Subjects item = adapter.getItem(position);
//                            Toast.makeText(Subject.this, "" + item.getId(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Subject.this, Lesson.class);
                            intent.putExtra("subject_id", item.getId());
                            intent.putExtra("subject_name", item.getName());
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<Subjects>> call, Throwable t) {
                Toast.makeText( Subject.this, "khong co du lieu nao", Toast.LENGTH_LONG).show();
            }
        });

    }
}
