package com.example.graduateteam.endgame.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.graduateteam.endgame.R;
import com.example.graduateteam.endgame.ResponseAPI.Carts;


import java.util.ArrayList;

public class CartAdapter extends ArrayAdapter<Carts> {
    Context mContext;
    int mLayout;
    ArrayList<Carts> mList;

    public CartAdapter( Context context, int resource, ArrayList<Carts> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mLayout = resource;
        this.mList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View item = convertView;
        if(item == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            item = inflater.inflate(this.mLayout, null);
        }


        Carts c = mList.get(position);

        TextView cart_course = (TextView) item.findViewById(R.id.cart_course);
        TextView cart_price = (TextView) item.findViewById(R.id.cart_price);



        cart_course.setText(c.getCourseName());
        cart_price.setText(c.getPrice() + " vnđ");

        return item;
    }
}
