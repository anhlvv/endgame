package com.example.graduateteam.endgame.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.graduateteam.endgame.R;
import com.example.graduateteam.endgame.ResponseAPI.Course;

import java.util.ArrayList;

public class CourseAdapter extends ArrayAdapter<Course> {
    Context mContext;
    int mLayout;
    ArrayList<Course> mList;

    public CourseAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<Course> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mLayout = resource;
        this.mList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View item = convertView;
        if(item == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            item = inflater.inflate(this.mLayout, null);
        }


        Course c = mList.get(position);

        TextView course_name = (TextView) item.findViewById(R.id.course_name);
        TextView course_des = (TextView) item.findViewById(R.id.course_des);
        TextView course_start = (TextView) item.findViewById(R.id.course_start);
        TextView course_end = (TextView) item.findViewById(R.id.course_end);
        TextView course_price = (TextView) item.findViewById(R.id.course_price);

        course_name.setText(c.getName());
        course_des.setText(c.getDescription());
        course_start.setText(c.getStartDate());
        course_end.setText(c.getEndDate());
        course_price.setText(c.getCoursePrice() + " vnđ");

        return item;
    }
}
