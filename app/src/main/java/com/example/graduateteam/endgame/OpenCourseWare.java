package com.example.graduateteam.endgame;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.graduateteam.endgame.Client.APIUtils;
import com.example.graduateteam.endgame.Client.DataClient;
import com.example.graduateteam.endgame.ResponseAPI.User;


import java.io.FileOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OpenCourseWare extends AppCompatActivity {
    VideoView video;
//    Button btnPlay;
    ProgressBar progressBar;
    MediaController mediaC;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_course_ware);
//        btnPlay = (Button) findViewById(R.id.playVideo);
        Intent intent = getIntent();
        String cwName = intent.getStringExtra("cwName");
        setTitle("Video: " + cwName);
        String cwfileName = intent.getStringExtra("cwfileName");
        video = findViewById(R.id.videoView);
        mediaC = new MediaController(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        Toast.makeText(OpenCourseWare.this, cwfileName, Toast.LENGTH_SHORT).show();
        try{
            video.setVideoPath("http://192.168.1.5:8080/EndGame/jsp/file/" + cwfileName);
            video.setMediaController(mediaC);
            video.start();
            video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }catch (Exception e){
            Toast.makeText(OpenCourseWare.this, "khong co video", Toast.LENGTH_SHORT).show();
        }
//        btnPlay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    video.setVideoPath("http://192.168.1.5:8080/EndGame/jsp/file/" + cwName);
//                    video.setMediaController(mediaC);
//                    video.start();
//                } catch (Exception e) {
//                    Toast.makeText(OpenCourseWare.this, "khong co video", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
    }
}
