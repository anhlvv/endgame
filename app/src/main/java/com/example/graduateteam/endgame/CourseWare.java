package com.example.graduateteam.endgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.graduateteam.endgame.Adapter.CourseWareAdapter;
import com.example.graduateteam.endgame.Client.APIUtils;
import com.example.graduateteam.endgame.Client.DataClient;
import com.example.graduateteam.endgame.ResponseAPI.CourseWares;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CourseWare extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_ware);

        Intent intent = getIntent();
        String lesson_name = intent.getStringExtra("lesson_name");
        setTitle("Bài học: " + lesson_name);
        int lesson_id = intent.getIntExtra("lesson_id", 0);
        Toast.makeText(CourseWare.this, "" + lesson_id, Toast.LENGTH_SHORT).show();

        loadData(lesson_id);
    }

    private void loadData(int lesson_id) {

        DataClient dataClient = APIUtils.getData();
        Call<List<CourseWares>> callback = dataClient.CourseWareData(lesson_id);

        callback.enqueue(new Callback<List<CourseWares>>() {
            @Override
            public void onResponse(Call<List<CourseWares>> call, Response<List<CourseWares>> response) {
                ArrayList<CourseWares> arrCourseWare = (ArrayList<CourseWares>) response.body();
                if (arrCourseWare.size() > 0) {
                    ListView listView = (ListView) findViewById(R.id.list_courseware);
                    final CourseWareAdapter adapter = new CourseWareAdapter(CourseWare.this, R.layout.item_courseware, arrCourseWare);
                    listView.setAdapter(adapter);

                    registerForContextMenu(listView);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            CourseWares item = adapter.getItem(position);
                            Toast.makeText(CourseWare.this, "" + item.getId(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(CourseWare.this, OpenCourseWare.class);
                            intent.putExtra("cwfileName", item.getLink());
                            intent.putExtra("cwName", item.getName());
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<CourseWares>> call, Throwable t) {

            }
        });

//        callback.enqueue(new Callback<List<Lessons>>() {
//            @Override
//            public void onResponse(Call<List<Lessons>> call, Response<List<Lessons>> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<List<Lessons>> call, Throwable t) {
//                Toast.makeText( Lesson.this, "khong co du lieu nao", Toast.LENGTH_LONG).show();
//            }
//        });

    }
}
