package com.example.graduateteam.endgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.graduateteam.endgame.Adapter.LessonAdapter;
import com.example.graduateteam.endgame.Client.APIUtils;
import com.example.graduateteam.endgame.Client.DataClient;
import com.example.graduateteam.endgame.ResponseAPI.Lessons;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Lesson extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson);
        Intent intent = getIntent();
        String subject_name = intent.getStringExtra("subject_name");
        setTitle("Môn học: " + subject_name);
        int subject_id = intent.getIntExtra("subject_id", 0);
        Toast.makeText(Lesson.this, "" + subject_id, Toast.LENGTH_SHORT).show();

        loadData(subject_id);
    }

    private void loadData(int course_id) {

        DataClient dataClient = APIUtils.getData();
        Call<List<Lessons>> callback = dataClient.LessonData(course_id);

        callback.enqueue(new Callback<List<Lessons>>() {
            @Override
            public void onResponse(Call<List<Lessons>> call, Response<List<Lessons>> response) {
                ArrayList<Lessons> arrLesson = (ArrayList<Lessons>) response.body();
                if (arrLesson.size() > 0) {
                ListView listView = (ListView) findViewById(R.id.list_lesson);
                    final LessonAdapter adapter = new LessonAdapter(Lesson.this, R.layout.item_lesson, arrLesson);
                    listView.setAdapter(adapter);
//
                    registerForContextMenu(listView);
//
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Lessons item = adapter.getItem(position);
//                            Toast.makeText(Lesson.this, "" + item.getId(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Lesson.this, CourseWare.class);
                            intent.putExtra("lesson_id", item.getId());
                            intent.putExtra("lesson_name", item.getName());
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<Lessons>> call, Throwable t) {
                Toast.makeText( Lesson.this, "khong co du lieu nao", Toast.LENGTH_LONG).show();
            }
        });

    }
}
