package com.example.graduateteam.endgame;


import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.graduateteam.endgame.Client.APIUtils;
import com.example.graduateteam.endgame.Client.DataClient;
import com.example.graduateteam.endgame.DAO.EGDAO;
import com.example.graduateteam.endgame.ResponseAPI.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Login extends AppCompatActivity {

    Retrofit retrofit;
    Button btnLogin, btnRegister;
    EditText edtUsername, edtPassword;
    String username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mapping();

        EGDAO egDao = new EGDAO(Login.this);
        ArrayList<User> lstUser = egDao.read();

        if(lstUser.size() > 0){
            Intent intent = new Intent(Login.this, Home.class);
            startActivity(intent);
            finish();
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = edtUsername.getText().toString();
                password = edtPassword.getText().toString();

                if(username.length() > 0 && password.length() > 0){
                    DataClient dataClient = APIUtils.getData();
                    Call<User> callback = dataClient.LoginData(username, password);
                    callback.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            User user = (User) response.body();
                            try{
                                EGDAO egDao = new EGDAO(Login.this);

                                long successAdd = egDao.insert(user);
                                Toast.makeText(Login.this, "" + successAdd, Toast.LENGTH_SHORT).show();
                                if (successAdd > 0) {
                                    Intent intent = new Intent(Login.this, Home.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }catch(Exception e){
                                Log.d("Exception", e.toString());

                                Toast.makeText(Login.this, "sai tai khoan hoac mat khau ", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {

                            Log.d("t: ", t.toString());
                            Toast.makeText(Login.this, "khong co tai khoan nay ", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Register.class);
                startActivity(intent);
            }
        });
    }

    private void mapping(){
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnRegister = (Button) findViewById(R.id.btn_register);
        edtUsername = (EditText) findViewById(R.id.input_email);
        edtPassword = (EditText) findViewById(R.id.input_password);
    }

}
