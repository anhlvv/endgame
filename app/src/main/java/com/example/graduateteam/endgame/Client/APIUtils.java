package com.example.graduateteam.endgame.Client;

public class APIUtils {
    public static final String Base_Url = "http://192.168.1.5:8080/ProjectEndGameApi/rest/";

    public static DataClient getData(){
        return RetrofitClient.getClient(Base_Url).create(DataClient.class);
    }
}
