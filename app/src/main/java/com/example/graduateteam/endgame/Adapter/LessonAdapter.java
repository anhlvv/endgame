package com.example.graduateteam.endgame.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.graduateteam.endgame.R;
import com.example.graduateteam.endgame.ResponseAPI.Lessons;

import java.util.ArrayList;

public class LessonAdapter extends ArrayAdapter<Lessons> {
    Context mContext;
    int mLayout;
    ArrayList<Lessons> mList;

    public LessonAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<Lessons> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mLayout = resource;
        this.mList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View item = convertView;
        if(item == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            item = inflater.inflate(this.mLayout, null);
        }


        Lessons s = mList.get(position);

        TextView lesson_name = (TextView) item.findViewById(R.id.lesson_name);
        TextView lesson_des = (TextView) item.findViewById(R.id.lesson_des);
        TextView lesson_createDate = (TextView) item.findViewById(R.id.lesson_createDate);

        lesson_name.setText(s.getName());
        lesson_des.setText(s.getDescription());
        lesson_createDate.setText(s.getCreateAt());

        return item;
    }
}
