package com.example.graduateteam.endgame.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.example.graduateteam.endgame.ResponseAPI.Carts;
import com.example.graduateteam.endgame.ResponseAPI.User;

import java.util.ArrayList;
import java.util.List;

public class EGDAO {
    Context mContext;
    SQLiteDatabase mDB;

    public EGDAO(Context mContext) {
        this.mContext = mContext;

        DatabaseHelper helper = new DatabaseHelper(mContext);

        mDB = helper.getWritableDatabase();

    }

    public ArrayList<User> read() {
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT * FROM data_user";
        Cursor cursor = mDB.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String username = cursor.getString(1);
            String password = cursor.getString(2);
            String firstName = cursor.getString(3);
            String lastName = cursor.getString(4);
            String displayName = cursor.getString(5);
            String email = cursor.getString(6);
            String birthday = cursor.getString(7);
            String phone = cursor.getString(8);
            Integer sex = cursor.getInt(9);
            Boolean status = cursor.getInt(10) == 1 ? true : false;
            String createAt = cursor.getString(11);
            String updateAt = cursor.getString(12);
            Integer createBy = cursor.getInt(13);
            Integer updateBy = cursor.getInt(14);
            Integer roleId = cursor.getInt(15);
            User user = new User(id, username, password, firstName, lastName, displayName, email, birthday, phone, sex, status, createAt, updateAt, createBy, updateBy, roleId);
            list.add(user);
        }
        return list;
    }

    public ArrayList<Carts> getAllCart(){
        ArrayList<Carts> list = new ArrayList<>();
        String sql = "SELECT * FROM data_cart";
        Cursor cursor = mDB.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String courseName = cursor.getString(1);
            int  courseId = cursor.getInt(2);
            int price = cursor.getInt(3);
            Carts cart = new Carts(courseName,id,courseId, price);
            list.add(cart);
        }
        return list;
    }

    public long insertCart(Carts c) {
        ContentValues cv = new ContentValues();
        ArrayList<Carts> lst = getAllCart();
//        for (Carts ca: lst) {
//            if(ca.getCourseName() == c.getCourseName()){
//                Toast.makeText(mContext, "Bạn đã thêm khóa học này trong giỏ hàng rồi ", Toast.LENGTH_SHORT).show();
//                return 0;
//            }else {
                cv.put("courseName", c.getCourseName());
                cv.put("id", c.getId());
                cv.put("courseId", c.getCourseId());
                cv.put("price", c.getPrice());
                long id = mDB.insert("data_cart", null, cv);
                return id;
//            }
//        }
//        return 0;
    }

    public void deleteCart(int idCart) {

        long id = mDB.delete("data_cart", "id=?", new String[]{String.valueOf(idCart)});
        Toast.makeText(mContext, "Xóa thành công Id : " + id, Toast.LENGTH_SHORT).show();
    }

    public boolean deleteAllCart() {
        boolean successDel = false;
        successDel = mDB.delete("data_cart", null, null) > 0;
        return successDel;
    }

    public int totalPrice(){
        List<Carts> list = getAllCart();
        int total = 0;
        for (Carts c:list) {
            total += c.getPrice();
        }
        return total;
    }
    public long insert(User user) {
        ContentValues cv = new ContentValues();
        cv.put("id", user.getId());
        cv.put("username", user.getUsername());
        cv.put("password", user.getPassword());
        cv.put("firstName", user.getFirstName());
        cv.put("lastName", user.getLastName());
        cv.put("displayName", user.getDisplayName());
        cv.put("email", user.getEmail());
        cv.put("birthday", user.getBirthday());
        cv.put("phone", user.getPhone());
        cv.put("sex", user.getSex());
        cv.put("status", user.getStatus());
        cv.put("createAt", user.getCreateAt());
        cv.put("updateAt", user.getUpdateAt());
        cv.put("createBy", user.getCreateBy());
        cv.put("updateBy", user.getUpdateBy());
        cv.put("roleId", user.getRoleId());
        long id = mDB.insert("data_user", null, cv);
        return id;
    }


    public boolean delete(int id) {
        boolean successDel = false;
        successDel = mDB.delete("data_user", "id='" + id + "'", null) > 0;
        if (successDel) {
            Toast.makeText(mContext, "Xóa thành công ID: " + id, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "Bản ghi chưa được xoá!", Toast.LENGTH_SHORT).show();
        }
        return successDel;
    }
}

